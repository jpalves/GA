clear
close all

n = 5;
x        = zeros(1,n);
y        = zeros(1,n);
vFitness = zeros(1,n);
for i = 1:n
    tic
    xb      = ga3(@fitness3,500,500,64,40,10,50,0.9,0.09,0.05,80,0.9,0.5,0.1,0.001,0.5,1);
    toc
    genoma  = xb(end,:);
    var1    = sum(gc2bin(genoma(1:32)).*2.^((length(genoma)/2-1):-1:0));
    var2    = sum(gc2bin(genoma(33:end)).*2.^((length(genoma)/2-1):-1:0));
    maxInt  = 2.^(length(genoma)/2)-1;
    v1      = (maxInt - var1)/maxInt;
    x(i)    = -3*(1-v1) + 3*v1;
    v2      = (maxInt - var2)/maxInt;
    y(i)    = -3*(1-v2) + 3*v2;
    vFitness(i)  = 100 * (x(i)^2 - y(i))^2 + (1 - x(i))^2;
    disp([median(x(1:i)),median(y(1:i)),median(vFitness(1:i))])
end

[mean(x),mean(y),mean(vFitness)]