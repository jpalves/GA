clear
close all
tic
xb      = ga3(@fitness2,100,20,32,4,10,5,0.9,0.09,0.05,10,0.9,0.5,0.1,0.01,0.5,1);
toc

f = @(x)x+10*sin(5*x)+7*cos(4*x);
varInt  = gc2dec(xb(end,:));
maxInt  = 2.^length(xb(end,:))-1;
zeroaum = (maxInt - varInt)/maxInt;
x1 = zeroaum*2*pi; 

x = 0:0.01:2*pi;
y = f(x);
figure()
plot(x,y)
hold on
plot(x1,f(x1),'or')
hold off
