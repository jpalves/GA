function vFitness = fitness1(genoma)
    var1  = sum(genoma(1:32).*2.^((length(genoma)/2-1):-1:0));
    var2  = sum(genoma(33:end).*2.^((length(genoma)/2-1):-1:0));
    maxInt  = 2.^(length(genoma)/2)-1;
	v1 = (maxInt - var1)/maxInt;
x  = -3*(1-v1) + 3*v1;
	
	v2 = (maxInt - var2)/maxInt;
y = -3*(1-v2) + 3*v2;
    
	vFitness  = 100 * (x^2 - y)^2 + (1 - x)^2;
    %vFitness = x + y;
