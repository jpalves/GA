function bin = gc2bin(g)
tamanho = size(g,2);%num of columns in the input vector
bin = logical(zeros(1,tamanho)); %#ok<LOGL>

for j = tamanho:-1:2 
    if rem(sum(g(1:j - 1)),2);
            bin(j) = ~g(j);
    else
            bin(j) =  g(j);
    end
end
bin(1) = g(1);
